# PPK-Praktikum 7 : Single Sign On (SSO)

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Teknologi Single-sign-on (sering disingkat menjadi SSO) adalah teknologi yang mengizinkan pengguna jaringan agar dapat mengakses sumber daya dalam jaringan hanya dengan menggunakan satu akun pengguna saja. Teknologi ini sangat diminati, khususnya dalam jaringan yang sangat besar dan bersifat heterogen (di saat sistem operasi serta aplikasi yang digunakan oleh komputer berasal dari banyak vendor, dan pengguna dimintai untuk mengisi informasi dirinya ke dalam setiap platform yang berbeda tersebut yang hendak diakses oleh pengguna). Dengan menggunakan SSO, seorang pengguna hanya cukup melakukan proses autentikasi sekali saja untuk mendapatkan izin akses terhadap semua layanan yang terdapat di dalam jaringan. 

Pada praktikum kali ini kita akan mempraktekkan SSO menggunakan SimpleSAMLphp untuk pada aplikasi web. SimpleSAMLphp adalah aplikasi autentikasi PHP yang bersifat terbuka dan mendukung SAML 2.0 sebagai Service Provider (SP) maupun sebagai Identity Provider (IdP). Website untuk SimpleSAMLphp terdapat di https://simplesamlphp.org/


## Kegiatan Praktikum

## 1. Installasi SimpleSamlphp
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-7/-/raw/master/screenshot/Screenshot%20(471).png)
## 2. Database Users
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-7/-/raw/master/screenshot/Screenshot%20(472).png)
## 3. Test Authentication Sources
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-7/-/raw/master/screenshot/Screenshot%20(473).png)
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-7/-/raw/master/screenshot/Screenshot%20(474).png)
## 4. Testing myweb1
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-7/-/raw/master/screenshot/Screenshot%20(475).png)
## 5. Testing myweb2
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-7/-/raw/master/screenshot/Screenshot%20(476).png)
